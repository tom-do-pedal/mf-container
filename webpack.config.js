const HtmlWebPackPlugin = require("html-webpack-plugin");
const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const { VueLoaderPlugin } = require("vue-loader");
const { getRemotesByEnv, getPublicPath } = require("./_configs/remotesConfig");

module.exports = (env) => {
  const envName = ['production', 'development'].find(envType => env[envType])
  const remotes = getRemotesByEnv(envName)
  const publicPath = getPublicPath(envName)

  return ({

    output: {
      publicPath,
    },

    resolve: {
      extensions: [".tsx", ".ts", ".vue", ".jsx", ".js", ".json"],
    },

    devServer: {
      port: 3000,
      historyApiFallback: true,
    },

    module: {
      rules: [
        {
          test: /\.vue$/,
          loader: "vue-loader",
          options: {
            hotReload: false,
          },
        },
        {
          test: /\.tsx?$/,
          use: [
            "babel-loader",
            {
              loader: "ts-loader",
              options: {
                transpileOnly: true,
                appendTsSuffixTo: ["\\.vue$"],
                happyPackMode: true,
              },
            },
          ],
        },
        {
          test: /\.(css|s[ac]ss)$/i,
          use: ["style-loader", "css-loader", "postcss-loader", "sass-loader"],
        },
      ],
    },

    plugins: [
      new VueLoaderPlugin(),
      new ModuleFederationPlugin({
        name: "container",
        filename: "remoteEntry.js",
        remotes,
        exposes: {},
        shared: require("./package.json").dependencies,
      }),
      new HtmlWebPackPlugin({
        template: "./src/index.html",
      }),
    ],
  })
};

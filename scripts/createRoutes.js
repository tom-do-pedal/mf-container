const fs = require('fs-extra')
const path = require('path')

const { appsConfig } = require('../_configs/containerConfig')

const routesTemplate = fs.readFileSync(path.resolve(__dirname, '../src/router/routes.template'), 'utf-8')

const routers = appsConfig.map(config => ({
  ...config.router,
  component: `___FUNCTION_START___() => import('../mfs/${config.id}.vue')___FUNCTION_END___`
}))
const routersParsed = JSON.stringify(routers, null, 2)
  .replaceAll('"___FUNCTION_START___', '')
  .replaceAll('___FUNCTION_END___"', '')
  .replace(/^\[/, '')
  .replace(/\]$/, '')

const routesFile = routesTemplate.replace("_____ROUTES_____", routersParsed)

fs.writeFileSync(path.resolve(__dirname, `../src/router/index.ts`), routesFile, 'utf-8')
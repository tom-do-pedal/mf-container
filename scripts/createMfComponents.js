const fs = require('fs-extra')
const path = require('path')

const { appsConfig } = require('../_configs/containerConfig')

const mfTemplate = fs.readFileSync(path.resolve(__dirname, '../src/mfs/mfTemplate.vue'), 'utf-8')

for (let i = 0; i < appsConfig.length; i++) {
  const config = appsConfig[i];
  const mfTemplateCopy = mfTemplate

  const configVueComponent = mfTemplateCopy
    .replaceAll("_____COMPONENT.ELEMENT-CLASS_____", config.component.elementClass)
    .replaceAll("_____COMPONENT.PATH_____", config.component.path)
    .replaceAll("_____MENU.LABEL_____", config.menu.label)

  fs.writeFileSync(path.resolve(__dirname, `../src/mfs/${config.id}.vue`), configVueComponent, 'utf-8')
}


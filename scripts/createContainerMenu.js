const fs = require('fs-extra')
const path = require('path')

const { appsConfig } = require('../_configs/containerConfig')

const menuTemplate = fs.readFileSync(path.resolve(__dirname, '../src/components/MainMenu/MainMenu.template.vue'), 'utf-8')

const menu = appsConfig.map(config => ({
  id: config.id,
  link: config.router.path,
  label: config.menu.label,
}))
const menuParsed = JSON.stringify(menu, null, 2)
  .replace(/^\[/, '')
  .replace(/\]$/, '')

const menuVueComponent = menuTemplate.replace("/*** MENU_ITEMS ***/", menuParsed)

fs.writeFileSync(path.resolve(__dirname, `../src/components/MainMenu/MainMenu.vue`), menuVueComponent, 'utf-8')
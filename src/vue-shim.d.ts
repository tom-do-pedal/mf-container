declare module "*.vue" {
  import { defineComponent } from "vue";
  const Component: ReturnType<typeof defineComponent>;
  export default Component;
}

declare module "home/HomeApp";
declare module "bike_routes/BikeRoutesApp";
import { createApp } from "vue";
import "tom_do_pedal_ui/CommomStyles";

import App from "./App.vue";
import { create } from "./router";

const router = create();
createApp(App)
  .use(router)
  .mount("#app");
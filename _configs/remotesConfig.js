const prodRemotes = {
  "home": "home@https://tom-do-pedal-mf-home.netlify.app/remoteEntry.js",
  "bike_routes": "bike_routes@https://tom-do-pedal-mf-bike-routes.netlify.app/remoteEntry.js",
  "tom_do_pedal_ui": "tom_do_pedal_ui@https://tom-do-pedal-ui.netlify.app/remoteEntry.js",
}

const devRemotes = {
  ...prodRemotes,
  // "home": "home@http://localhost:3002/remoteEntry.js",
  // "bike_routes": "bike_routes@http://localhost:3001/remoteEntry.js",
  // "tom_do_pedal_ui": "tom_do_pedal_ui@http://localhost:8181/remoteEntry.js",
}

module.exports = {
  getRemotesByEnv: (env) => env === 'production' ? prodRemotes : devRemotes,
  getPublicPath: (env) => env === 'production' ? "https://tom-do-pedal.netlify.app/" : "http://localhost:3000/"
}
module.exports = {
  appsConfig: [
    {
      id: 'home',
      component: {
        path: 'home/HomeApp',
        elementClass: 'mf-home'
      },
      menu: {
        label: 'Home'
      },
      router: {
        path: '/',
        alias: ['/home'],
        name: 'home',
      }
    },
    {
      id: 'bike-routes',
      component: {
        path: 'bike_routes/BikeRoutesApp',
        elementClass: 'mf-bike-routes',
      },
      menu: {
        label: 'Routes'
      },
      router: {
        path: '/bike-routes',
        alias: ['/bike-routes', '/bike-routes/:id', '/bike-rotas', '/bike-rotas/:id'],
        name: 'bike-routes',
      }
    }
  ]
}